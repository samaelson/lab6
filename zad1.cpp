#include <fcntl.h>
#include <stdio.h>
#define MAX 512
 int main(int argc, char* argv[]) {
           char buf[MAX];
           int desc_zrod, desc_cel;
           int lbajt;
 
           if (argc<3) { // użyj standardowego wyjścia komunikatów błędów
             fprintf(stderr, "Za malo argumentow. Uzyj:\n");
             fprintf(stderr, "%s <pliki zrodlowe> ... <plik docelowy>\m", argv[0]);
             exit(1);
           }
 
           desc_cel = creat(argv[argc], 0640);
           if (desc_cel == -1){
             perror("Blad utworzenia pliku docelowego");
             exit(1);
           }
 
           for (int i = 2,i<argc;i++) {
             desc_zrod = open(argv[i], O_RDONLY);
             if (desc_zrod == -1) { // plik nie został otwarty
               perror("Blad otwarcia pliku");
               exit(1);
             }
             
             while((lbajt = read(desc_zrod, buf, MAX)) > 0) {
               if (write(desc_cel, buf, lbajt) == -1){
                perror("Blad zapisu pliku docelowego");
                exit(1);
               }
             }
             if (lbajt == -1) {
               perror("Blad odczytu pliku zrodlowego");
               exit(1);
             }
           }
           exit(0);
         }
