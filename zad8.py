#!/usr/bin/env python
import os

def my_fork():
    child_pid = os.fork()
    if child_pid == 0:
        print ("Child Process: PID# %s" % os.getpid())
    else:
        print ("Parent Process: PID# %s" % os.getpid())
    child2_pid = os.fork()
    if child_pid == 0:
        print ("Child 2 Process: PID# %s" % os.getpid())
    else:
        print ("Parent Process: PID# %s" % os.getpid())
    child3_pid = os.fork()
    if child_pid == 0:
        print ("Child3 Process: PID# %s" % os.getpid())
    else:
        print ("Parent Process: PID# %s" % os.getpid())
    child4_pid = os.fork()
    if child_pid == 0:
        print ("Child4 Process: PID# %s" % os.getpid())
    else:
        print ("Parent Process: PID# %s" % os.getpid())

if __name__ == "__main__":
    my_fork()
    os.system("sleep 120")