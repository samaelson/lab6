#!/usr/bin/python
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('-i', '--input', help='Input file name', required=True)
parser.add_argument('-o', '--output', help='Output file name', required=True)
args = parser.parse_args()
print("Input file: %s" % args.input)
print("Output file: %s" % args.output)
source = open(args.input, "r")
destination = open(args.output, "a")
linelist = source.readlines()
linelist = linelist[-10:]

def lastnines():
    destination.write("Wypisuje 10 Ostatnich Lini \n")
    for line in linelist:
        destination.write(line)
    destination.write("\n END")
def lastword():
    counter = 0
    destination.write("\n Wypisuje 10 ostatnich słów \n")
    for word in reversed(open(args.input, "r").read().split()):
        destination.write(word)
        destination.write("\n")
        counter+=1
        if counter == 10:
            destination.write("\n END \n")
            break


def lastchar():
    counter = 0
    destination.write("\n Wypisuje 10 ostatnich znaków \n")
    for word in reversed(open(args.input, "r").read().split()):
        wordlist=list(word)
        for each in wordlist:
            destination.write(each)
            destination.write("\n")
            counter+=1
            print(counter)
            if counter == 10:
                destination.write("\n END \n")
                exit()



lastnines()
lastword()
lastchar()
source.close()
destination.close()