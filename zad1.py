#!/usr/bin/python
import argparse
parser = argparse.ArgumentParser(description='') 
parser.add_argument('-i','--input', help='Input file name',required=True)
parser.add_argument('-o','--output',help='Output file name', required=True)
args = parser.parse_args()
 
## show values ##
print ("Input file: %s" % args.input )
print ("Output file: %s" % args.output )
source = open(args.input , "r")
destination = open(args.output , "w")
for line in source:
	destination.write(line)
source.close()
destination.close()
