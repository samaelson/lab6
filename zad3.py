#!/usr/bin/python
import sys

for arg in sys.argv[1:-2]:
    soruce = open(arg, "r")
    destination = open(sys.argv[-1], "a")
    for line in soruce:
        destination.write(line)
    soruce.close()
    destination.close()
